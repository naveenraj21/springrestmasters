package spring.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import spring.course.model.Zoo;
import spring.course.service.ZooService;

@RestController
@RequestMapping("/zoo")
public class ZooController {

	@Autowired
	ZooService zooService;

	@RequestMapping(value = "/zoo/", method = RequestMethod.GET)
	public ResponseEntity<List<Zoo>> getAllZoo() {
		List<Zoo> zoos = zooService.findAllZoos();
		if (null == zoos || zoos.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Zoo>>(zoos, HttpStatus.OK);
	}

	@RequestMapping(value = "/zoo/{id}", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getStud(@PathVariable("id") long id) {
		Zoo zoo = zooService.findById(id);
		if (zoo == null) {
			return new ResponseEntity(new RestResponse("NOT_FOUND", "Zoo with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<RestResponse>(new RestResponse("SUCCESS", zoo), HttpStatus.OK);
	}

	@RequestMapping(value = "/zoo/", method = RequestMethod.POST)
	public ResponseEntity<?> createZoo(@RequestBody Zoo zoo, UriComponentsBuilder ucBuilder) {

		if (zooService.isZooExist(zoo)) {
			return new ResponseEntity(new RestResponse("Unable to create. A Zoo with name " + zoo + " already exist."),
					HttpStatus.CONFLICT);
		}
		zooService.saveZoo(zoo);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/endpoint/zoo/{id}").buildAndExpand(zoo.getZooId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/zoo/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateZoo(@PathVariable("id") long id, @RequestBody Zoo zoo) {
		Zoo resultZoo = zooService.findById(id);

		if (resultZoo == null) {
			return new ResponseEntity(new RestResponse("Unable to upate. Zoo with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		zooService.updateZoo(resultZoo);
		return new ResponseEntity<Zoo>(resultZoo, HttpStatus.OK);
	}

	@RequestMapping(value = "/zoo/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteZoo(@PathVariable("id") long id) {

		Zoo zoo = zooService.findById(id);
		if (zoo == null) {
			return new ResponseEntity(new RestResponse("Unable to delete. Zoo with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		zooService.deleteZooById(id);
		return new ResponseEntity<Zoo>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/zoo/", method = RequestMethod.DELETE)
	public ResponseEntity<Zoo> deleteAllZoos() {
		zooService.deleteAllzoos();
		return new ResponseEntity<Zoo>(HttpStatus.NO_CONTENT);
	}

}
