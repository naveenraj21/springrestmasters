package spring.course.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import spring.course.model.Zoo;

@Repository
public interface ZooRepository extends JpaRepository<Zoo, Long> {

	Zoo findByZooName(String name);

}
