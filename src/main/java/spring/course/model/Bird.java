package spring.course.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "BIRD")
public class Bird implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BIRD_ID", unique = true, nullable = false)
	private Integer birdId;

	@Column(name = "BD1")
	private String bd1;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "bird")
	private Set<Seed> seedList = new HashSet<Seed>(0);

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ZOO_ID", nullable = false)
	private Zoo zoo;

	public Integer getBirdId() {
		return birdId;
	}

	public void setBirdId(Integer birdId) {
		this.birdId = birdId;
	}

	public String getBd1() {
		return bd1;
	}

	public void setBd1(String bd1) {
		this.bd1 = bd1;
	}

	public Set<Seed> getSeedList() {
		return seedList;
	}

	public void setSeedList(Set<Seed> seedList) {
		this.seedList = seedList;
	}

	public Zoo getZoo() {
		return zoo;
	}

	public void setZoo(Zoo zoo) {
		this.zoo = zoo;
	}
}
