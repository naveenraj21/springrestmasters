package spring.course.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ZOO")
public class Zoo implements Serializable {

	@Id
	@Column(name = "Zoo_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long zooId;

	@Column(name = "ZOO_NAME")
	private String zooName;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "zoo")
	private Set<Animal> animalList = new HashSet<Animal>(0);

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "zoo")
	private Set<Bird> birdList = new HashSet<Bird>(0);

	public Long getZooId() {
		return zooId;
	}

	public void setZooId(Long zooId) {
		this.zooId = zooId;
	}

	public String getZooName() {
		return zooName;
	}

	public void setZooName(String zooName) {
		this.zooName = zooName;
	}

	public Set<Animal> getAnimalList() {
		return animalList;
	}

	public void setAnimalList(Set<Animal> animalList) {
		this.animalList = animalList;
	}

	public Set<Bird> getBirdList() {
		return birdList;
	}

	public void setBirdList(Set<Bird> birdList) {
		this.birdList = birdList;
	}

}
