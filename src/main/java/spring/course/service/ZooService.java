package spring.course.service;
	
import java.util.List;

import spring.course.model.Zoo;

public interface ZooService {
	
	Zoo findById(Long id);

	Zoo findByZooName(String name);

	void saveZoo(Zoo zoo);

	void updateZoo(Zoo zoo);

	void deleteZooById(Long id);

	void deleteAllzoos();

	List<Zoo> findAllZoos();

	boolean isZooExist(Zoo zoo);
}
