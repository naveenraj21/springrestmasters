package spring.course.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spring.course.model.Animal;
import spring.course.model.Bird;
import spring.course.model.Zoo;
import spring.course.repositories.ZooRepository;

@Service("zooService")
@Transactional
public class ZooServiceImpl implements ZooService {

	@Autowired
	private ZooRepository zooRepository;

	@Override
	public Zoo findById(Long id) {
		// TODO Auto-generated method stub
		return zooRepository.findOne(id);
	}

	@Override
	public Zoo findByZooName(String name) {
		// TODO Auto-generated method stub
		return zooRepository.findByZooName(name);
	}

	@Override
	public void saveZoo(Zoo zoo) {
		// TODO Auto-generated method stub
		if (null != zoo.getAnimalList()) {
			zoo.getAnimalList().forEach((animal) -> animal.setZoo(zoo));

			Animal animal = null;
			if (null != animal.getFoodList()) {
				animal.getFoodList().forEach((food) -> food.setAnimal(animal));
			}
		}
		if (null != zoo.getBirdList()) {
			zoo.getBirdList().forEach((bird) -> bird.setZoo(zoo));

			Bird bird;
			if (null != bird.getSeedList()) {
				bird.getSeedList().forEach((seed) -> seed.setBird(bird));
			}
		}
		zooRepository.save(zoo);
	}

	@Override
	public void updateZoo(Zoo zoo) {
		// TODO Auto-generated method stub
		saveZoo(zoo);

	}

	@Override
	public void deleteZooById(Long id) {
		// TODO Auto-generated method stub
		zooRepository.delete(id);
	}

	@Override
	public void deleteAllzoos() {
		// TODO Auto-generated method stub
		zooRepository.deleteAll();
	}

	@Override
	public List<Zoo> findAllZoos() {
		// TODO Auto-generated method stub
		return zooRepository.findAll();
	}

	@Override
	public boolean isZooExist(Zoo zoo) {
		// TODO Auto-generated method stub
		return findByZooName(zoo.getZooName()) != null;
	}
}
